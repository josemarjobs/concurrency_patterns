package main

import (
	"fmt"
	"time"
)

func run() {
	po := &PurchaseOrderCb{Value: 42.27}

	ch := make(chan *PurchaseOrderCb)

	go SavePOCb(po, ch)

	newPo := <-ch

	fmt.Println(newPo)
}

type PurchaseOrderCb struct {
	Number int
	Value  float64
}

func SavePOCb(po *PurchaseOrderCb, callback chan *PurchaseOrderCb) {
	time.Sleep(500 * time.Millisecond)
	po.Number = 123
	callback <- po
}
