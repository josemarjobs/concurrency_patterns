package main

import "fmt"

func run() {
	btn := MakeButton()

	handlerOne := make(chan string)
	handlerTwo := make(chan string)

	btn.AddEventListener("click", handlerOne)
	btn.AddEventListener("click", handlerTwo)

	go func() {
		for {
			select {
			case msg := <-handlerOne:
				fmt.Println("Handler One:", msg)
			case msg := <-handlerTwo:
				fmt.Println("Handler Two:", msg)
			}
		}
	}()

	btn.TriggerEvent("click", "Button Clicked")
	btn.removeEventListener("click", handlerTwo)

	btn.TriggerEvent("click", "Button Clicked")

	fmt.Scanln()
}

type Button struct {
	eventListeners map[string][]chan string
}

func MakeButton() *Button {
	return &Button{
		eventListeners: make(map[string][]chan string),
	}
}

func (this *Button) AddEventListener(event string, responseChannel chan string) {
	if _, present := this.eventListeners[event]; present {
		this.eventListeners[event] = append(this.eventListeners[event], responseChannel)
	} else {
		this.eventListeners[event] = []chan string{responseChannel}
	}
}

func (this *Button) removeEventListener(event string, listenerChannel chan string) {
	if _, present := this.eventListeners[event]; present {
		for idx, _ := range this.eventListeners[event] {
			if this.eventListeners[event][idx] == listenerChannel {
				this.eventListeners[event] = append(this.eventListeners[event][:idx], this.eventListeners[event][idx+1:]...)
				break
			}
		}
	}
}

func (this *Button) TriggerEvent(event string, response string) {
	if _, present := this.eventListeners[event]; present {
		for _, handler := range this.eventListeners[event] {
			go func(handler chan string) {
				handler <- response
			}(handler)
		}
	}
}
