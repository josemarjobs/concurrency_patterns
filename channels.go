package main

import "fmt"

func run() {
	msgCh := make(chan Message, 1)
	errCh := make(chan FailedMessage, 1)

	msg := Message{
		To:      []string{"frodo@underhill.me"},
		From:    "gandalf@whitecouncil.org",
		Content: "keep it secret, keep it safe",
	}

	failedMessage := FailedMessage{
		ErrorMessage:    "Message intercepted by black rider",
		OriginalMessage: msg,
	}

	msgCh <- msg
	errCh <- failedMessage

	select {
	case m := <-msgCh:
		fmt.Println(m)
	case err := <-errCh:
		fmt.Println(err)
	default:
		fmt.Println("No msg")
	}
}

type Message struct {
	To      []string
	From    string
	Content string
}
type FailedMessage struct {
	ErrorMessage    string
	OriginalMessage Message
}
