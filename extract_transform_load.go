package main

import (
	"encoding/csv"
	"fmt"
	"os"
	"strconv"
	"sync"
	"time"
)

func main() {
	start := time.Now()

	extractCh := make(chan *Order)
	transformCh := make(chan *Order)
	doneCh := make(chan struct{})

	go extract(extractCh)
	go transform(extractCh, transformCh)
	go load(transformCh, doneCh)

	<-doneCh
	fmt.Println("Elapsed time:", time.Since(start))
}

type Product struct {
	PartNumber string
	UnitCost   float64
	UnitPrice  float64
}

type Order struct {
	CustomerNumber int
	PartNumber     string
	Quantity       int

	UnitCost  float64
	UnitPrice float64
}

func panicIf(err error) {
	if err != nil {
		panic(err)
	}
}

func extract(ch chan *Order) {
	f, err := os.Open("./orders.txt")
	panicIf(err)
	defer f.Close()
	r := csv.NewReader(f)

	for record, err := r.Read(); err == nil; record, err = r.Read() {
		order := new(Order)
		order.CustomerNumber, _ = strconv.Atoi(record[0])
		order.PartNumber = record[1]
		order.Quantity, _ = strconv.Atoi(record[2])
		ch <- order
	}

	close(ch)
}

func transform(extractCh, transformCh chan *Order) {
	f, err := os.Open("./productList.txt")
	panicIf(err)
	defer f.Close()
	r := csv.NewReader(f)

	records, _ := r.ReadAll()

	productList := make(map[string]*Product)
	for _, record := range records {
		product := new(Product)
		product.PartNumber = record[0]
		product.UnitCost, _ = strconv.ParseFloat(record[1], 64)
		product.UnitPrice, _ = strconv.ParseFloat(record[2], 64)
		productList[product.PartNumber] = product
	}

	var wg sync.WaitGroup
	for o := range extractCh {
		wg.Add(1)
		go func(o *Order) {
			defer wg.Done()
			time.Sleep(3 * time.Millisecond)
			o.UnitCost = productList[o.PartNumber].UnitCost
			o.UnitPrice = productList[o.PartNumber].UnitPrice
			transformCh <- o
		}(o)
	}

	wg.Wait()
	close(transformCh)
}

func load(transformCh chan *Order, doneCh chan struct{}) {
	f, err := os.Create("./dest.txt")
	panicIf(err)
	defer f.Close()

	fmt.Fprintf(f, "%11s%15s%12s%12s%15s%15s\n",
		"Part Number", "Quantity", "Unit Cost",
		"Unit Price", "Total Cost", "Total Price")

	wg := sync.WaitGroup{}
	for o := range transformCh {
		wg.Add(1)
		go func(o *Order) {
			defer wg.Done()
			time.Sleep(1 * time.Millisecond)
			fmt.Fprintf(f, "%11s%15d%12.2f%12.2f%15.2f%15.2f\n",
				o.PartNumber, o.Quantity, o.UnitCost, o.UnitPrice,
				o.UnitCost*float64(o.Quantity), o.UnitPrice*float64(o.Quantity))
		}(o)
	}

	wg.Wait()
	doneCh <- struct{}{}
}
