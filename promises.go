package main

import (
	"errors"
	"fmt"
	"time"
)

func run() {
	po := &PurchaseOrder{Value: 42.27}

	SavePO(po, false).Then(func(obj interface{}) error {
		po := obj.(*PurchaseOrder)
		time.Sleep(1 * time.Second)
		fmt.Println(po)
		return nil
	}, func(err error) {
		fmt.Println(err)
	}).Then(func(obj interface{}) error {
		fmt.Println("Second handler:", obj)
		return nil
	}, func(err error) {
		fmt.Println("Second handler:", err)
	})

	fmt.Scanln()
}

type PurchaseOrder struct {
	Number int
	Value  float64
}

func SavePO(po *PurchaseOrder, shouldFail bool) *Promise {
	result := &Promise{
		successChannel: make(chan interface{}, 1),
		failureChannel: make(chan error, 1),
	}

	go func() {
		if shouldFail {
			result.failureChannel <- errors.New("Failed to save PO")
		} else {
			po.Number = 132
			result.successChannel <- po
		}
	}()

	return result
}

type Promise struct {
	successChannel chan interface{}
	failureChannel chan error
}

type SuccessFunc func(interface{}) error
type ErrorFunc func(error)

func (this *Promise) Then(success SuccessFunc, failure ErrorFunc) *Promise {
	result := &Promise{
		successChannel: make(chan interface{}, 1),
		failureChannel: make(chan error, 1),
	}
	timeout := time.After(2 * time.Second)
	go func() {
		select {
		case obj := <-this.successChannel:
			if newErr := success(obj); newErr == nil {
				result.successChannel <- obj
			} else {
				result.failureChannel <- newErr
			}
		case err := <-this.failureChannel:
			failure(err)
			result.failureChannel <- err
		case <-timeout:
			err := errors.New("Timeout")
			failure(err)
			result.failureChannel <- err
		}
	}()

	return result
}
